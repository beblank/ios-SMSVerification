//
//  ViewController.swift
//  SMSVerification
//
//  Created by aditya on 7/30/16.
//  Copyright © 2016 aditya. All rights reserved.
//

import UIKit;
import SinchVerification;

class ViewController: UIViewController {

    var verification:Verification!
    var applicationKey:String = "97066907-dc99-49bc-bb70-910a2630aa5a"
    
    @IBOutlet weak var calloutButton:UIButton!
    @IBOutlet weak var phoneNumber:UITextField!
    @IBOutlet weak var smsButton:UIButton!
    @IBOutlet weak var statusText:UILabel!
    @IBOutlet weak var spinner:UIActivityIndicatorView!
    
    func disableUI(disable:Bool){
        var alpha:CGFloat = 1.0
        if (disable){
            alpha = 0.5
            phoneNumber.resignFirstResponder()
            spinner.startAnimating()
            self.statusText.text = ""
            let delayTime = dispatch_time(DISPATCH_TIME_NOW, Int64(30*Double(NSEC_PER_MSEC)))
            dispatch_after(delayTime, dispatch_get_main_queue(), { () -> Void in
                self.disableUI(false)}
            )
        } else {
            self.phoneNumber.becomeFirstResponder()
            self.spinner.stopAnimating()
            
        }
        self.phoneNumber.enabled = !disable
        self.calloutButton.enabled = !disable
        self.smsButton.enabled = !disable
        self.calloutButton.alpha = alpha
        self.smsButton.alpha = alpha
    }
    
    override func viewDidLoad() {
        super.viewDidLoad()
        // Do any additional setup after loading the view, typically from a nib.
    }
    
    @IBAction func callout(sender: AnyObject){
        disableUI(true)
        verification = CalloutVerification(applicationKey: applicationKey, phoneNumber: phoneNumber.text!)
        verification.initiate{(success:Bool, error:NSError?) -> Void in
            self.disableUI(false)
            self.statusText.text = success ? "Verified" : error?.description
        }
    }
    
    @IBAction func sms(sender: AnyObject){
         disableUI(true)
        verification = SMSVerification(applicationKey: applicationKey, phoneNumber: phoneNumber.text!)
        verification.initiate{(success:Bool, error:NSError? ) -> Void in
            self.disableUI(false);
            if (success){
                 self.performSegueWithIdentifier("enterPin", sender: sender)
            } else {
                self.statusText.text = error?.description;
            }
           
        }
    }
    
    override func prepareForSegue(segue: UIStoryboardSegue, sender: AnyObject?) {
        if (segue.identifier == "enterPin"){
            let enterCodeVC = segue.destinationViewController as! EnterCodeViewController
            enterCodeVC.verification = self.verification
        }
    }

}

