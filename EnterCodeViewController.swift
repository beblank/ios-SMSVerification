//
//  EnterCodeViewController.swift
//  SMSVerification
//
//  Created by aditya on 8/1/16.
//  Copyright © 2016 aditya. All rights reserved.
//

import UIKit
import SinchVerification

class EnterCodeViewController: UIViewController {

    var verification:Verification!
    var applicationKey:String = "97066907-dc99-49bc-bb70-910a2630aa5a"
    
    @IBOutlet weak var pinButton: UIButton!
    @IBOutlet weak var enterPin: UITextField!
    @IBOutlet weak var status:UILabel!
    
    override func viewDidLoad() {
        super.viewDidLoad()

        // Do any additional setup after loading the view.
    }

    override func didReceiveMemoryWarning() {
        super.didReceiveMemoryWarning()
        // Dispose of any resources that can be recreated.
    }
    
    @IBAction func verify(sender: AnyObject){
        enterPin.enabled = false
        verification.verify(enterPin.text!, completion: {(succes:Bool, error:NSError?) -> Void in
            
            if (succes){
                self.status.text = "Verified"
            } else {
                self.status.text = error?.description
            }
        })
    }
}
